//
//  SmiSdkFramework.h
//  SmiSdkFramework
//
//  Created by Damandeep Singh on 23/08/17.
//  Copyright © 2017 Damandeep Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SmiSdk.h"
#import "SmiAnalytics.h"

//! Project version number for SmiSdkFramework.
FOUNDATION_EXPORT double SmiSdkFrameworkVersionNumber;

//! Project version string for SmiSdkFramework.
FOUNDATION_EXPORT const unsigned char SmiSdkFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SmiSdkFramework/PublicHeader.h>


