mkdir iphoneos
mkdir iphonesimulator
xcodebuild -sdk "iphoneos" -scheme SmiSdkFramework -configuration Release  -arch arm64 -arch arm64e -arch armv7 -arch armv7s only_active_arch=no archive -archivePath $PWD/build/SmiSdkFramework.xcarchive
cp -r build/SmiSdkFramework.xcarchive/Products/Library/Frameworks/SmiSdkFramework.framework iphoneos

xcodebuild -sdk "iphonesimulator" -scheme SmiSdkFramework -configuration Release -arch i386 -arch x86_64 only_active_arch=no archive -archivePath $PWD/build/SmiSdkFrameworkSim.xcarchive
cp -r build/SmiSdkFrameworkSim.xcarchive/Products/Library/Frameworks/SmiSdkFramework.framework iphonesimulator

lipo -create iphoneos/SmiSdkFramework.framework/SmiSdkFramework iphonesimulator/SmiSdkFramework.framework/SmiSdkFramework -output SmiSdkFramework.framework/SmiSdkFramework